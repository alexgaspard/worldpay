package com.example.offers.domain.services;

import com.example.offers.domain.OfferPersister;
import com.example.offers.domain.OffersFinder;
import com.example.offers.domain.exceptions.AlreadyExpired;
import com.example.offers.domain.exceptions.NotCancelled;
import com.example.offers.domain.exceptions.NotCreated;
import com.example.offers.domain.model.Offer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

class OffersServiceTest {

    private OffersFinder finder;
    private OfferPersister persister;
    private OffersService service;

    @BeforeEach
    void setUp() {
        finder = mock(OffersFinder.class);
        persister = mock(OfferPersister.class);
        service = new OffersService(finder, persister);
    }

    @Test
    void createOfferShouldPersistOfferWithEmptyId() throws NotCreated {
        final String description = "description";
        final Instant startDate = Instant.now();
        final Duration validityPeriod = Duration.ofMinutes(1);
        final Currency currency = Currency.getInstance(Locale.CANADA);
        final double price = 1.2;
        final boolean cancelled = true;
        final String savedId = "savedId";
        when(persister.persist(any())).thenReturn(savedId);
        final String id = service.createOffer(new Offer("id", description, startDate, validityPeriod, currency, price, cancelled));
        assertEquals(savedId, id);
        final ArgumentCaptor<Offer> captor = ArgumentCaptor.forClass(Offer.class);
        verify(persister).persist(captor.capture());
        assertTrue(captor.getValue().getId().isEmpty());
        assertEquals(description, captor.getValue().getDescription());
        assertEquals(startDate, captor.getValue().getStartDate());
        assertEquals(validityPeriod, captor.getValue().getValidityPeriod());
        assertEquals(currency, captor.getValue().getCurrency());
        assertEquals(price, captor.getValue().getPrice());
        assertEquals(cancelled, captor.getValue().isCancelled());
    }

    @Test
    void createOfferWhenExpiredShouldThrowException() {
        final Duration validityPeriod = Duration.ofMinutes(1);
        final Instant startDate = Instant.now().minus(validityPeriod).minusSeconds(5);
        assertThrows(NotCreated.class,
                () -> service.createOffer(new Offer("", "", startDate, validityPeriod, Currency.getInstance(Locale.CANADA), 1.2, true)));
        verifyZeroInteractions(persister);
    }

    @Test
    void getOfferShouldFind() {
        final String id = "id";
        final Optional<Offer> offerOptional = Optional.of(new Offer(id, "", null, null, null, 0, false));
        when(finder.find(id)).thenReturn(offerOptional);
        final Optional<Offer> result = service.getOffer(id);
        assertEquals(offerOptional, result);
        verify(finder).find(id);
    }

    @Test
    void listOffersShouldFindAll() {
        final String id = "id";
        final Collection<Offer> offers = Collections.singleton(new Offer(id, "", null, null, null, 0, false));
        when(finder.findAll()).thenReturn(offers);
        final Collection<Offer> result = service.listOffers();
        assertEquals(offers, result);
        verify(finder).findAll();
    }

    @Test
    void cancelOfferShouldPersistOfferWithTrueCancelledAndSameId() throws NotCancelled {
        final String id = "id";
        final String description = "description";
        final Instant startDate = Instant.now();
        final Duration validityPeriod = Duration.ofMinutes(1);
        final Currency currency = Currency.getInstance(Locale.CANADA);
        final double price = 1.2;
        when(finder.find(any())).thenReturn(Optional.of(new Offer(id, description, startDate, validityPeriod, currency, price, false)));
        service.cancelOffer(id);
        verify(finder).find(id);
        final ArgumentCaptor<Offer> captor = ArgumentCaptor.forClass(Offer.class);
        verify(persister).persist(captor.capture());
        assertEquals(id, captor.getValue().getId());
        assertEquals(description, captor.getValue().getDescription());
        assertEquals(startDate, captor.getValue().getStartDate());
        assertEquals(validityPeriod, captor.getValue().getValidityPeriod());
        assertEquals(currency, captor.getValue().getCurrency());
        assertEquals(price, captor.getValue().getPrice());
        assertTrue(captor.getValue().isCancelled());
    }

    @Test
    void cancelOfferWhenNotFoundShouldThrowException() {
        final String id = "id";
        when(finder.find(any())).thenReturn(Optional.empty());
        assertThrows(NotCancelled.class,
                () -> service.cancelOffer(id));
        verify(finder).find(id);
        verifyZeroInteractions(persister);
    }

    @Test
    void cancelOfferWhenExpiredShouldThrowException() {
        final String id = "id";
        final Duration validityPeriod = Duration.ofMinutes(1);
        final Instant startDate = Instant.now().minus(validityPeriod).minusSeconds(5);
        when(finder.find(any())).thenReturn(Optional.of(new Offer(id, "", startDate, validityPeriod, null, 0, false)));
        assertThrows(AlreadyExpired.class,
                () -> service.cancelOffer(id));
        verify(finder).find(id);
        verifyZeroInteractions(persister);
    }
}