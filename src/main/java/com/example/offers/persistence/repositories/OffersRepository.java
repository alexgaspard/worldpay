package com.example.offers.persistence.repositories;

import com.example.offers.persistence.model.OfferEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OffersRepository extends CrudRepository<OfferEntity, Long> {

}
