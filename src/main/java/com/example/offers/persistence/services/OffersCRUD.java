package com.example.offers.persistence.services;

import com.example.offers.domain.OfferPersister;
import com.example.offers.domain.OffersFinder;
import com.example.offers.domain.model.Offer;
import com.example.offers.persistence.model.OfferEntity;
import com.example.offers.persistence.repositories.OffersRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OffersCRUD implements OfferPersister, OffersFinder {

    private final OffersRepository repository;

    public OffersCRUD(OffersRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Offer> find(String id) {
        return repository.findById(convertIdFromStringToLong(id)).map(this::convertToOffer);
    }

    @Override
    public Collection<Offer> findAll() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .map(this::convertToOffer)
                .collect(Collectors.toList());
    }

    @Override
    public String persist(Offer offer) {
        return Long.toString(repository.save(new OfferEntity(convertIdFromStringToLong(offer.getId()), offer.getDescription(), offer.getStartDate(),
                offer.getValidityPeriod(), offer.getCurrency(), offer.getPrice(), offer.isCancelled())).getId());
    }

    private long convertIdFromStringToLong(String id) {
        if (id.isEmpty()) {
            return 0;
        }
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private Offer convertToOffer(OfferEntity entity) {
        return new Offer(Long.toString(entity.getId()), entity.getDescription(), entity.getStartDate(), entity.getValidityPeriod(),
                entity.getCurrency(), entity.getPrice(), entity.isCancelled());
    }
}
