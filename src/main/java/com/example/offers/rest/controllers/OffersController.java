package com.example.offers.rest.controllers;

import com.example.offers.domain.OfferCanceller;
import com.example.offers.domain.OfferCreator;
import com.example.offers.domain.OfferGetter;
import com.example.offers.domain.OffersLister;
import com.example.offers.domain.exceptions.NotCancelled;
import com.example.offers.domain.exceptions.NotCreated;
import com.example.offers.domain.model.Offer;
import com.example.offers.rest.exceptions.NotFound;
import com.example.offers.rest.model.OfferJSON;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(OffersController.OFFERS_PATH)
public class OffersController {

    static final String OFFERS_PATH = "/offers";
    static final String ID_PATH = "/{id}";

    private final OfferCreator offerCreator;
    private final OfferGetter offerGetter;
    private final OffersLister offersLister;
    private final OfferCanceller offerCanceller;

    public OffersController(OfferCreator offerCreator, OfferGetter offerGetter, OffersLister offersLister, OfferCanceller offerCanceller) {
        this.offerCreator = offerCreator;
        this.offerGetter = offerGetter;
        this.offersLister = offersLister;
        this.offerCanceller = offerCanceller;
    }

    @GetMapping
    public Collection<OfferJSON> listOffers() {
        return offersLister.listOffers().stream().map(this::convertToJSON).collect(Collectors.toList());
    }

    @GetMapping(ID_PATH)
    public OfferJSON getOffer(@PathVariable String id) {
        return offerGetter.getOffer(id).map(this::convertToJSON)
                .orElseThrow(NotFound::new);
    }

    @PostMapping
    public ResponseEntity<Void> createOffer(@RequestBody @Valid OfferJSON offer) {
        try {
            final String savedId = offerCreator.createOffer(new Offer(offer.getId(), offer.getDescription(), offer.getStartDate(), offer.getValidityPeriod(),
                    offer.getCurrency(), offer.getPrice(), offer.isCancelled()));
            final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(ID_PATH).buildAndExpand(savedId).toUri();
            return ResponseEntity.created(location).build();
        } catch (NotCreated e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping(ID_PATH + "/cancellation")
    public ResponseEntity<Void> cancelOffer(@PathVariable String id) {
        try {
            offerCanceller.cancelOffer(id);
        } catch (NotCancelled e) {
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        }
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().replacePath(OFFERS_PATH + ID_PATH).buildAndExpand(id).toUri();
        return ResponseEntity.status(HttpStatus.SEE_OTHER).location(location).build();
    }

    private OfferJSON convertToJSON(Offer offer) {
        return new OfferJSON(offer.getId(), offer.getDescription(), offer.getStartDate(), offer.getValidityPeriod(), offer.getCurrency(),
                offer.getPrice(), offer.isCancelled(), offer.isExpired());
    }
}
