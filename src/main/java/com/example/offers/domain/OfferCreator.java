package com.example.offers.domain;

import com.example.offers.domain.exceptions.NotCreated;
import com.example.offers.domain.model.Offer;

public interface OfferCreator {

    String createOffer(Offer offer) throws NotCreated;
}
