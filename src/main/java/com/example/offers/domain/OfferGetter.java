package com.example.offers.domain;

import com.example.offers.domain.model.Offer;

import java.util.Optional;

public interface OfferGetter {

    Optional<Offer> getOffer(String id);
}
