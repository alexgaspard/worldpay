package com.example.offers.domain.services;

import com.example.offers.domain.OfferCanceller;
import com.example.offers.domain.OfferCreator;
import com.example.offers.domain.OfferGetter;
import com.example.offers.domain.OfferPersister;
import com.example.offers.domain.OffersFinder;
import com.example.offers.domain.OffersLister;
import com.example.offers.domain.exceptions.AlreadyExpired;
import com.example.offers.domain.exceptions.NotCancelled;
import com.example.offers.domain.exceptions.NotCreated;
import com.example.offers.domain.model.Offer;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class OffersService implements OfferCreator, OfferGetter, OffersLister, OfferCanceller {

    private final OffersFinder finder;
    private final OfferPersister persister;

    public OffersService(OffersFinder finder, OfferPersister persister) {
        this.finder = finder;
        this.persister = persister;
    }

    @Override
    public String createOffer(Offer offer) throws NotCreated {
        if (offer.isExpired()) {
            throw new NotCreated();
        }
        return persister.persist(new Offer(offer, ""));
    }

    @Override
    public Optional<Offer> getOffer(String id) {
        return finder.find(id);
    }

    @Override
    public Collection<Offer> listOffers() {
        return finder.findAll();
    }

    @Override
    public void cancelOffer(String id) throws NotCancelled {
        final Offer offer = finder.find(id).orElseThrow(NotCancelled::new);
        if (offer.isExpired()) {
            throw new AlreadyExpired();
        }
        persister.persist(new Offer(offer, true));
    }
}
