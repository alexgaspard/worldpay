package com.example.offers.domain;

import com.example.offers.domain.model.Offer;

public interface OfferPersister {

    String persist(Offer offer);
}
