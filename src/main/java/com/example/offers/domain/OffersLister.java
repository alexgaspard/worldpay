package com.example.offers.domain;

import com.example.offers.domain.model.Offer;

import java.util.Collection;

public interface OffersLister {

    Collection<Offer> listOffers();
}
