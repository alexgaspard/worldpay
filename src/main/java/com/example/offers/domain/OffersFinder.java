package com.example.offers.domain;

import com.example.offers.domain.model.Offer;

import java.util.Collection;
import java.util.Optional;

public interface OffersFinder {

    Optional<Offer> find(String id);

    Collection<Offer> findAll();
}
