package com.example.offers.domain.config;

import com.example.offers.domain.OfferPersister;
import com.example.offers.domain.model.Offer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.time.Instant;
import java.util.Currency;
import java.util.Locale;

@Configuration
public class DemoConfiguration {

    @Bean
    public CommandLineRunner demoData(OfferPersister persister) {
        return args -> {
            persister.persist(new Offer("", "First generated offer", Instant.now(), Duration.ofMinutes(2), Currency.getInstance(Locale.UK), 12.34, false));
            persister.persist(new Offer("", "Second generated offer", Instant.now().minusSeconds(50), Duration.ofMinutes(1), Currency.getInstance(Locale.UK), 56.78, true));
        };
    }
}
