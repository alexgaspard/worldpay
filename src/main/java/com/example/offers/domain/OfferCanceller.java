package com.example.offers.domain;

import com.example.offers.domain.exceptions.NotCancelled;

public interface OfferCanceller {

    void cancelOffer(String id) throws NotCancelled;
}
