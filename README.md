# Offers manager

This SpringBoot application allows to create, list, get and cancel offers.
An offer has a starting date and a validity period after which it will be expired and cannot get cancelled anymore. 

This project follows Hexagonal Architecture.

Available endpoints are:
```
curl -i localhost:8080/offers
curl -i localhost:8080/offers/1
curl -i localhost:8080/offers -H "Content-Type: application/json" -d '{"description":"test offer","startDate":"2019-09-19T23:59:37.274Z","validityPeriod":600.000000000,"currency":"GBP","price":99.00, "id": 3}'
curl -i -X PUT localhost:8080/offers/3/cancellation
```